package buzz;

import java.awt.BorderLayout;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;

public class ConsoleDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8639104874085901173L;
	private final JPanel contentPanel = new JPanel();
	private JScrollPane scrollPane;
	private JTextPane textPane;

	/**
	 * Create the dialog.
	 * @param jFrame 
	 */
	public ConsoleDialog(JFrame jFrame) {
		super(jFrame);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			scrollPane = new JScrollPane();
			{
				textPane = new JTextPane();
				scrollPane.setViewportView(textPane);
			}
		}
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addComponent(scrollPane, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 424, Short.MAX_VALUE)
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addGap(5)
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 213, Short.MAX_VALUE))
		);
		contentPanel.setLayout(gl_contentPanel);
	}

	public JTextPane getTextPane() {
		return textPane;
	}
}
