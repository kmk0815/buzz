package buzz;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Point;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URI;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import com.sun.javafx.application.PlatformImpl;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import net.java.games.input.Component;
import net.java.games.input.Controller;
import net.java.games.input.ControllerEnvironment;
import net.java.games.input.Event;
import net.java.games.input.EventQueue;

public class BuzzApp {

	private MediaPlayer clip;
	private JFrame jFrame = null; // @jve:decl-index=0:visual-constraint="10,10"
	private JPanel jContentPane = null;
	private JMenuBar jJMenuBar = null;
	private JMenu fileMenu = null;
	private JMenu helpMenu = null;
	private JMenuItem exitMenuItem = null;
	private JMenuItem aboutMenuItem = null;
	private JDialog aboutDialog = null;
	private JPanel aboutContentPane = null;
	private JLabel aboutVersionLabel = null;

	private static String buttons[] = new String[] { "Taste 10", "Taste 5", "Taste 0", "Taste 15" };
	private JLabel buzzLabel = null;
	private Clip buzzerSound;
	private Media curretClip;
	private JMenuItem playMenuItem = null;
	private JMenu musikMenu = null;
	private JMenu menusMenu = null;
	private JMenuItem menu1Item = null;
	private JMenuItem menu2Item = null;
	private JMenuItem menu3Item = null;
	private JMenuItem stopMenuItem = null;
	private JCheckBox chckbxNewCheckBox;

	public BuzzApp() {
		File f = new File("src/main/resources/BUZZER.WAV");
		try {
			buzzerSound = AudioSystem.getClip();
			buzzerSound.open(AudioSystem.getAudioInputStream(f));
		} catch (IOException | UnsupportedAudioFileException | LineUnavailableException e) {
			e.printStackTrace();
		}

	}

	/**
	 * This method initializes jFrame
	 * 
	 * @return javax.swing.JFrame
	 */
	private JFrame getJFrame() {
		if (jFrame == null) {
			jFrame = new JFrame();
			jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			jFrame.setFont(new Font("Dialog", Font.PLAIN, 12));
			jFrame.setJMenuBar(getJJMenuBar());
			jFrame.setSize(1000, 700);
			jFrame.setContentPane(getJContentPane());
			jFrame.setTitle("Schalg den PArtnER BUZZ!!!");
		}
		return jFrame;
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			GridBagConstraints gridBagConstraints = new GridBagConstraints();
			gridBagConstraints.gridx = 0;
			gridBagConstraints.gridy = 0;
			buzzLabel = new JLabel();
			buzzLabel.setText("");
			buzzLabel.setFont(new Font("Arial Black", Font.BOLD, 200));
			jContentPane = new JPanel();
			jContentPane.setLayout(new GridBagLayout());
			jContentPane.add(buzzLabel, gridBagConstraints);
		}
		return jContentPane;
	}

	/**
	 * This method initializes jJMenuBar
	 * 
	 * @return javax.swing.JMenuBar
	 */
	private JMenuBar getJJMenuBar() {
		if (jJMenuBar == null) {
			jJMenuBar = new JMenuBar();
			jJMenuBar.add(getFileMenu());
			jJMenuBar.add(getMusikMenu());
			jJMenuBar.add(getMenusMenu());
			jJMenuBar.add(getHelpMenu());
			jJMenuBar.add(getChckbxNewCheckBox());
		}
		return jJMenuBar;
	}

	/**
	 * This method initializes jMenu
	 * 
	 * @return javax.swing.JMenu
	 */
	private JMenu getFileMenu() {
		if (fileMenu == null) {
			fileMenu = new JMenu();
			fileMenu.setText("File");
			fileMenu.add(getExitMenuItem());
		}
		return fileMenu;
	}

	/**
	 * This method initializes jMenu
	 * 
	 * @return javax.swing.JMenu
	 */
	private JMenu getHelpMenu() {
		if (helpMenu == null) {
			helpMenu = new JMenu();
			helpMenu.setText("Help");
			helpMenu.add(getAboutMenuItem());
		}
		return helpMenu;
	}

	/**
	 * This method initializes jMenuItem
	 * 
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getExitMenuItem() {
		if (exitMenuItem == null) {
			exitMenuItem = new JMenuItem();
			exitMenuItem.setText("Exit");
			exitMenuItem.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					System.exit(0);
				}
			});
		}
		return exitMenuItem;
	}

	/**
	 * This method initializes jMenuItem
	 * 
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getAboutMenuItem() {
		if (aboutMenuItem == null) {
			aboutMenuItem = new JMenuItem();
			aboutMenuItem.setText("About");
			aboutMenuItem.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					JDialog aboutDialog = getAboutDialog();
					aboutDialog.pack();
					Point loc = getJFrame().getLocation();
					loc.translate(20, 20);
					aboutDialog.setLocation(loc);
					aboutDialog.setVisible(true);
				}
			});
		}
		return aboutMenuItem;
	}

	/**
	 * This method initializes aboutDialog
	 * 
	 * @return javax.swing.JDialog
	 */
	private JDialog getAboutDialog() {
		if (aboutDialog == null) {
			aboutDialog = new JDialog(getJFrame(), true);
			aboutDialog.setTitle("About");
			aboutDialog.setContentPane(getAboutContentPane());
		}
		return aboutDialog;
	}

	/**
	 * This method initializes aboutContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getAboutContentPane() {
		if (aboutContentPane == null) {
			aboutContentPane = new JPanel();
			aboutContentPane.setLayout(new BorderLayout());
			aboutContentPane.add(getAboutVersionLabel(), BorderLayout.CENTER);
		}
		return aboutContentPane;
	}

	/**
	 * This method initializes aboutVersionLabel
	 * 
	 * @return javax.swing.JLabel
	 */
	private JLabel getAboutVersionLabel() {
		if (aboutVersionLabel == null) {
			aboutVersionLabel = new JLabel();
			aboutVersionLabel.setText("Polterabend Ren� & Sabine");
			aboutVersionLabel.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return aboutVersionLabel;
	}

	/**
	 * This method initializes playMenuItem
	 * 
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getPlayMenuItem() {
		if (playMenuItem == null) {
			playMenuItem = new JMenuItem();
			playMenuItem.setText("Play last");
			playMenuItem.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					if (curretClip != null) {
						clip.play();
					}
				}
			});
		}
		return playMenuItem;
	}

	/**
	 * This method initializes musikMenu
	 * 
	 * @return javax.swing.JMenu
	 */
	private JMenu getMusikMenu() {
		if (musikMenu == null) {
			musikMenu = new JMenu();
			musikMenu.setText("Musik");
			musikMenu.add(getPlayMenuItem());
			musikMenu.add(getStopMenuItem());
			File dir = new File("src/main/");
			for (final String s : dir.list(new FilenameFilter() {

				@Override
				public boolean accept(File dir, String name) {
					return !name.contains(".");
				}
			})) {
				JMenuItem jMenuItem = new JMenu(s);
				int i = 0;
				for (final String titel : new File(dir, s).list(new FilenameFilter() {

					@Override
					public boolean accept(File dir, String name) {
						return name.endsWith(".mp3");
					}
				})) {
					if (i++ > 23)
						break;
					// File titelFile = new File(s + titel);
					JMenuItem mMenuItem = new JMenuItem(titel);
					mMenuItem.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent arg0) {
							if (clip != null) {
								clip.stop();
								clip.dispose();
							}
							File f = new File(dir.toString() + "/" + s + (chckbxNewCheckBox.isSelected() ? "" : "/R"),
									titel);
							String string = f.toURI().toString();
							System.out.println(string + ":" + f.exists());
							curretClip = new Media(string);
							clip = new MediaPlayer(curretClip);

							clip.play();
						}
					});
					jMenuItem.add(mMenuItem);
				}
				musikMenu.add(jMenuItem);
			}
		}
		return musikMenu;
	}

	/**
	 * This method initializes menusMenu
	 * 
	 * @return javax.swing.JMenu
	 */
	private JMenu getMenusMenu() {
		if (menusMenu == null) {
			menusMenu = new JMenu();
			menusMenu.setText("Men�s");
			menusMenu.add(getMenu1Item());
			menusMenu.add(getMenu2Item());
			menusMenu.add(getMenu3Item());
		}
		return menusMenu;
	}

	/**
	 * This method initializes menu1Item
	 * 
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getMenu1Item() {
		if (menu1Item == null) {
			menu1Item = new JMenuItem();
			menu1Item.setText("Men� 1");
			menu1Item.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					buzzLabel.setFont(new Font("Arial Black", Font.BOLD, 120));
					buzzLabel.setText("<html>Olive<br>Banane<br>Lakritz<br>Mais</html>");
				}
			});
		}
		return menu1Item;
	}

	/**
	 * This method initializes menu2Item
	 * 
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getMenu2Item() {
		if (menu2Item == null) {
			menu2Item = new JMenuItem();
			menu2Item.setText("Men� 2");
			menu2Item.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					buzzLabel.setFont(new Font("Arial Black", Font.BOLD, 120));
					buzzLabel.setText("<html>Birne<br>Kaper<br>Karamell<br>Gurke</html>");
				}
			});
		}
		return menu2Item;
	}

	/**
	 * This method initializes menu3Item
	 * 
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getMenu3Item() {
		if (menu3Item == null) {
			menu3Item = new JMenuItem();
			menu3Item.setText("Men� 3");
			menu3Item.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					buzzLabel.setFont(new Font("Arial Black", Font.BOLD, 120));
					buzzLabel.setText("<html>Traube<br>Marzipan<br>Senf<br>Blumenkohl</html>");
				}
			});
		}
		return menu3Item;
	}

	/**
	 * This method initializes stopMenuItem
	 * 
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getStopMenuItem() {
		if (stopMenuItem == null) {
			stopMenuItem = new JMenuItem();
			stopMenuItem.setText("Stop");
			stopMenuItem.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					if (curretClip != null)
						clip.stop();
				}
			});
		}
		return stopMenuItem;
	}

	/**
	 * Launches this application
	 */
	public static void main(String[] args) {
		PlatformImpl.startup(() -> {
		});
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				BuzzApp application = new BuzzApp();
				application.getJFrame().setVisible(true);
				new Thread(new Runnable() {

					@Override
					public void run() {
						try {

							/* Create an event object for the underlying plugin to populate */
							Event event = new Event();

							/* Get the available controllers */
							Controller[] controllers = ControllerEnvironment.getDefaultEnvironment().getControllers();
							Controller c = null;
							for (int i = 0; i < controllers.length; i++) {
								if (controllers[i].getName().toLowerCase().contains("buzz")) {
									c = controllers[i];
								}
							}

							while (c != null) {
								/* Remember to poll each one */
								c.poll();

								/* Get the controllers event queue */
								EventQueue queue = c.getEventQueue();

								/* For each object in the queue */
								while (queue.getNextEvent(event)) {
									/* Get event component */
									Component comp = event.getComponent();

									/* Process event (your awesome code) */
									System.out.println(comp + ":" + comp.getPollData());
									int random = Math.round((float) Math.random() * 4f);
									for (int k = 0; k < buttons.length; k++) {
										int index = k + random;
										if (index >= buttons.length)
											index -= buttons.length;
										if (comp.getName().contentEquals(buttons[index]) && comp.getPollData() == 1) {
											System.out.println("Buzz " + index);
											if (index > 1) {
												application.buzzLabel.setFont(new Font("Arial Black", Font.BOLD, 150));
												application.buzzLabel.setText("Team Rot");
												application.getJContentPane().setBackground(Color.RED);
											} else {
												application.buzzLabel.setFont(new Font("Arial Black", Font.BOLD, 150));
												application.buzzLabel.setText("Team Blau");
												application.getJContentPane().setBackground(Color.BLUE);
											}
											try {
												if (application.curretClip != null)
													application.clip.stop();
												application.buzzerSound.loop(1);
												;
												Thread.sleep(5000);
												System.out.println("Bereit:");
												application.buzzLabel.setText("");
												application.getJContentPane().setBackground(SystemColor.control);

											} catch (InterruptedException e) {
											}
											break;

										}
									}
								}
							}
						} catch (UnsatisfiedLinkError e) {
							ConsoleDialog cd = new ConsoleDialog(application.getJFrame());
							cd.setTitle(e.getMessage());
							cd.getTextPane().setText(System.getProperty("java.library.path").replaceAll(";", "\n"));
							cd.setVisible(true);
							cd.setModal(true);
							System.out.println(System.getProperty("java.library.path"));
						}
					}
				}).start();

			}
			// try {
			// Joystick buzz = Joystick.createInstance();
			// buzz.setPollInterval(5);
			// System.out.println("Bereit:");
			// buzz.addJoystickListener(new JoystickListener() {
			//
			// @Override
			// public void joystickButtonChanged(Joystick j) {
			// int random = Math.round((float) Math.random() * 4f);
			// int button = j.getButtons();
			// for (int k = 0; k < buttons.length; k++) {
			// int index = k + random;
			// if (index >= buttons.length)
			// index -= buttons.length;
			// if ((button & buttons[index]) != 0) {
			// System.out.println("Buzz " + index);
			// if (index > 1) {
			// application.buzzLabel.setFont(new Font("Arial Black", Font.BOLD, 150));
			// application.buzzLabel.setText("Team Rot");
			// application.getJContentPane().setBackground(Color.RED);
			// } else {
			// application.buzzLabel.setFont(new Font("Arial Black", Font.BOLD, 150));
			// application.buzzLabel.setText("Team Blau");
			// application.getJContentPane().setBackground(Color.BLUE);
			// }
			// try {
			// if (application.curretClip != null)
			// application.clip.stop();
			// application.buzzerSound.start();
			// Thread.sleep(5000);
			// System.out.println("Bereit:");
			// application.buzzLabel.setText("");
			// application.getJContentPane().setBackground(SystemColor.control);
			//
			// } catch (InterruptedException e) {
			// }
			// break;
			// }
			// }
			// }
			//
			// @Override
			// public void joystickAxisChanged(Joystick j) {
			// }
			// });
			// } catch (UnsatisfiedLinkError | IOException e) {
			// ConsoleDialog cd = new ConsoleDialog(application.getJFrame());
			// cd.setTitle(e.getMessage());
			// cd.getTextPane().setText(System.getProperty("java.library.path").replaceAll(";",
			// "\n"));
			// cd.setVisible(true);
			// cd.setModal(true);
			// System.out.println(System.getProperty("java.library.path"));
			// }
			// }
		});
		PlatformImpl.exit();
	}

	private JCheckBox getChckbxNewCheckBox() {
		if (chckbxNewCheckBox == null) {
			chckbxNewCheckBox = new JCheckBox("Vorw\u00E4rts");
		}
		return chckbxNewCheckBox;
	}
}
