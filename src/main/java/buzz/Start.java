package buzz;

import java.io.IOException;

import javax.swing.JOptionPane;

import com.centralnexus.input.Joystick;
import com.centralnexus.input.JoystickListener;

public class Start {

	private static int buttons[] = new int[] { Joystick.BUTTON1,
			Joystick.BUTTON6, Joystick.BUTTON11, Joystick.BUTTON16 };

	public Start() {
		
		
		try {
			Joystick buzz = Joystick.createInstance();
			buzz.setPollInterval(5);
			System.out.println("Bereit:");
			buzz.addJoystickListener(new JoystickListener() {

				@Override
				public void joystickButtonChanged(Joystick j) {
					int random = Math.round((float) Math.random() * 4f);
					int button = j.getButtons();
					for (int k = 0; k < buttons.length; k++) {
						int index = k + random;
						if (index >= buttons.length)
							index -= buttons.length;
						if ((button & buttons[index]) != 0) {
							System.out.println("Buzz " + index);
							try {
								Thread.sleep(2000);
								System.out.println("Bereit:");
							} catch (InterruptedException e) {
							}
							break;
						}
					}
				}

				@Override
				public void joystickAxisChanged(Joystick j) {
				}
			});
		} catch (IOException e) {
			e.printStackTrace();
		} catch (UnsatisfiedLinkError e) {
			JOptionPane.showInternalMessageDialog(null, e.getMessage() + "\n" + System.getProperty("java.library.path"));
			e.printStackTrace();
			System.out.println(System.getProperty("java.library.path"));
        }
	}

	public static void main(String[] args) {
		new Start();
	}

}
